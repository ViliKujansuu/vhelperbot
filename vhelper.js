const { StaticAuthProvider } = require('@twurple/auth');
const fetch = require('node-fetch');

const tmi = require('tmi.js');

// Create an authprovider using client ID and app access token.
const clientId = process.env.CLIENT_ID;
const accessToken = process.env.APP_ACCESS;
const authProvider = new StaticAuthProvider(clientId, accessToken);

// Variables for whether a certain mode is on for the bot.
var vetmode = false;
var delmode = false;
var strict = false;

// Extra list for delmode. Lists new users that have tripped the bot during this session.
var blacklist = [];

var od = new Date("1970-01-030T00:00:00Z");
const month = od.getTime();


// Contains access information for the bot account.
const opts = {
  identity: {
    username: process.env.BOT_USERNAME,
    password: process.env.OAUTH_TOKEN
  },
  channels: [
    process.env.CHANNEL_NAME
  ]
}

// Creates a client for the bot.
const client = new tmi.client(opts);

// Sets the client to handle different events.
client.on('message', onMessageHandler);
client.on('connected', onConnectedHandler);
client.connect();

// Gets information in JSON form from the Twitch API
function getJSON(link) {
  return fetch(link, {
    method: 'get',
    headers: {
      'Authorization': `Bearer ${process.env.APP_ACCESS}`,
      'Client-ID': process.env.CLIENT_ID
    }
  })
  .then(function(response) {
      return response.json();
    }
  );
}

// Triggers every time someone posts a message.
async function onMessageHandler(target, context, msg, self) {
  
  // Ignore the bot's messages.
  if (self) {return;}
  
  // Format the message to be handled more easily as a command..
  const command = msg.trim();
  const args = command.split(" ");
  const first = command.charAt(0);
  
  // If vetmode is on check if it's the user's first message.
  if (vetmode & context['first-msg']) {
    
    // Get the user info from the API.
    var link = `https://api.twitch.tv/helix/users?login=${context.username}`;
    var userinfo = await getJSON(link);
    
    // Find out the account's age.
    var creation = new Date(userinfo.data[0].created_at);
    var ctime = creation.getTime();
    var now = new Date();
    var ntime = await now.getTime();
    var age = ntime - ctime;
    
    // If strict mode is on, time out the new user.
    if (strict) {
      client.timeout(target, context.username, 10, "There has been suspicious activity in the chat." + 
                   " As a precaution new users need to wait a bit before chatting. Sorry for the inconvenience.");
    }
    
    // If strict mode is off, time out only accounts that are less than month old.
    else if (age < month) {
      client.timeout(target, context.username, 10, "There has been suspicious activity in the chat." + 
                   " As a precaution new users need to wait a bit before chatting. Sorry for the inconvenience.");
    }
  }
  
  
  if (delmode) {
    
    // Get the user info and account age like before.
    var link = `https://api.twitch.tv/helix/users?login=${context.username}`;
    var userinfo = await getJSON(link);
    var creation = new Date(userinfo.data[0].created_at);
    var ctime = creation.getTime();
    var now = new Date();
    var ntime = await now.getTime();
    var age = ntime - ctime;
    
    // Delete messages from the new user or a blacklisted users. New users get added to the blacklist
    // so that the bot can keep track of their messages.
    if (strict & (context['first-msg'] | blacklist.includes(context.username))) {
      if (!blacklist.includes(context.username)) {
        blacklist.push(context.username);
      }
      client.deletemessage(target, context.id);
    }
    
    else if (age < month & (context['first-msg'] | blacklist.includes(context.username))) {
      if (!blacklist.includes(context.username)) {
        blacklist.push(context.username);
      }
      client.deletemessage(target, context.id);
      console.log(blacklist);
    }
  }
  
  // Checks if the message is a command.
  if (first === '!') {
    
    // Chooses right action based on the command.
    switch (args[0]) {
        
      // Displays a basic message to show the bot is working.
      case '!help':
        client.say(target, `Hello! I am VHelperBot. How can I help you?`);
        break;
      
      // Activates vetmode.
      case '!vetmode':
        // Checks that the command is invoked either by the channel owner or a moderator.
        if (context.username === target.replace('#', '') || context.mod) {
        vetmode = true;
          if (args.length === 2 & args[1] === 'strict') {
            strict = true;
          }
        }
        break;
      
      // Activates delmode.
      case '!delmode':
        if (context.username === target.replace('#', '') || context.mod) {
          console.log("Delmode enabled!");
          delmode = true;
          if (args.length === 2 & args[1] === 'strict') {
            strict = true;
          }
        }
        break;
        
      // Adds an user to the whitelist.
      case '!whitelist':
        if ((context.username === target.replace('#', '') || context.mod) && blacklist.includes(args[1])) {
          var index = blacklist.indexOf(args[1]);
          blacklist.splice(index);
          console.log(`${args[1]} has been whitelisted.`);
        }
      
      // Turns off delmode.
      case '!delmodeoff':
        if (context.username === target.replace('#', '') || context.mod) {
          delmode = false;
          strict = false;
        }
        break;
      
      // Turns off vetmode.
      case '!vetmodeoff':
        if (context.username === target.replace('#', '') || context.mod) {
          vetmode = false;
          strict = false;
        }
      
      // If none of the commands apply, send this error to console.
      default:
        console.log(`Unknown command. ${command}`)
    }
  }
}

// Show message in console to prove the client has connected.
function onConnectedHandler(addr, port) {
  console.log(`* Connected to ${addr}:${port}`)
}
