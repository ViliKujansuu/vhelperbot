# VHelper Bot

This bot was made as a practice project and as such it is not planned for actual use in its current state. The goal of the bot is to help chat moderation in twitch.tv, mainly focusing on new users coming into chat to send hateful messages. The project was originally located in Glitch, but has now been moved to GitLab.

The bot contains two modes: vetmode and delmode. Vetmode times out the first message a new user sends to chat for 1 minute. During that time the moderators can review the message to see, if the user joined in with malicious intentions. After that, if the user is not deemed suspicious, they can chat normally. When the bot is in delmode, it will delete any messages from new users instantly. Individual new users can be whitelisted to avoid their messages getting deleted.

The modes normally delete only the messages of new users that have created their account within a month. If you want it to affect older accounts, you can set the bot into strict mode, in which case it affects all new accounts in your chatroom.

Commands:
!help - Test command to see, if the bot is working.
!vetmode [strict] - Sets the bot on vetmode.
!delmode [strict] - Sets the bot on delmode.
!vetmodeoff - Turns vetmode off (and strict mode if it was on).
!delmodeoff - Turns delmode off (and strict mode if it was on).
!whitelist [user] - Allows a specific user to bypass delmode.
